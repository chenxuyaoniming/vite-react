import { defineConfig, resolveConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import { resolve } from 'path';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh()],
  resolve: {
    alias: {
      'Src': resolve('src'),
      'Components': resolve('src/components'),
      'Utils': resolve('src/utils'),
    }
  },
  server: {
    hmr: {
      overlay: true
    },
    proxy: {
      '^/api': {
        target: 'http://www.baidu.com',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ''),
      }
    }
  }
})
