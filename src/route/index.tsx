import React, {lazy, useEffect, useRef} from 'react';
import { BrowserRouter , Route, Switch} from 'react-router-dom';
import { useHistory } from 'react-router';
import Layout from 'Components/Layout';

const Router = () => {
    const history = useHistory();
    const routerListen = useRef(() => {});
    // 监听路由
    useEffect(() => {
        routerListen.current = history.listen(location => {
            console.log(location, 'location');
        })
    }, [])
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={lazy(() => import('../views/Index'))} />
                <Route path='/personCenter' component={lazy(() => import('../views/PersonCenter'))} />
                <Route path='/recommend' component={lazy(() => import('../views/Recommend'))} />
            </Switch>
        </Layout>
    )
}
export default Router;