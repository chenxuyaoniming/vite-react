import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter , Route, Switch} from 'react-router-dom'
import './index.css'
import App from './route'
import { Provider } from 'react-redux';
import store from './store';
ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
          <React.Suspense fallback={<div>loading......</div>}>
            <App />
          </React.Suspense>
      </BrowserRouter>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
)
