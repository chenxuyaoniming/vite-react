import React, { ReactElement, useEffect, useRef, useState } from 'react'
import './index.less'
import Dialog from 'Components/Dialog';
import { useDispatch, useSelector } from 'react-redux';
import { getFeedList } from '../../store/modules/common/actionCreator';

function App() {
  const [count, setCount] = useState(0)
  const feed = useSelector((state: Record<string, any>) => state.common?.feed);
  const dispatch = useDispatch();
  const handleClick = () => {
    setCount(1)
  }
  const getUserInfo = () => {
    dispatch(getFeedList());
  }

  useEffect(() => {
    return () => {
      setCount(0);
    }
  }, [])

  return (
    <div className="App" onClick={handleClick}>
      <Dialog isOpen={count > 0}>
        {
          <div>
            portal is good
          </div>
        }
      </Dialog> 
      <h1 onClick={getUserInfo}>点击获取</h1>
      <h4>{feed}</h4>
    </div>
  )
}

export default App
