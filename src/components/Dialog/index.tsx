import React, {useEffect, useRef, useState} from 'react';
import ReactDOM from 'react-dom';
import './index.less';

interface PropsType extends Props {
    isOpen: boolean
  }
const Dialog = ({children, isOpen}: PropsType) => {
    const ref = useRef<HTMLElement>();
  
    useEffect(() => {
      if (ref.current) {
        return;
      }
      ref.current = document.createElement('div');
      ref.current.classList.add('cf-dialog');
      document.body.appendChild(ref.current);
      return ()=> {
        document.body.removeChild(ref.current as Node);
      }
    }, [])
  
    return (ref.current 
        ? ReactDOM.createPortal((
            <div className={'cf-dialog-content'} aria-hidden={isOpen}>
              {children}
            </div>
          ), ref.current) 
        : null)
  }

export default Dialog;