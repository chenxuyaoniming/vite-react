import React from 'react';
import { useHistory } from 'react-router-dom';
import './index.less';

interface PropsTypes extends Props {
    navList?: Array<Record<string, any>>
}

const SideNav = (props: PropsTypes) => {
    const {navList} = props;
    const history = useHistory();

    const handleClick = (path: string) => {
        history.push(path);
    }

    return <div className="side-nav">
        {
            navList && navList.length && navList.map((item, index) => (
                <div 
                    className='side-nav-item' 
                    key={`${item.path}${index}`} 
                    onClick={() => handleClick(item.path)}>
                    {item.name}
                </div>
            ))
        }
    </div>
}

export default SideNav;