import React from 'react';
import './index.less';
import SideNav from '../SideNav';

const Layout = (props: Props) => {
    const { children } = props;
    const navList = [
        {path: '/', name: '主页'},
        {path: '/personCenter', name: '个人中心'},
        {path: '/recommend', name: '消息中心'},
    ]
    return (
        <div className="layout">
            <div className="layout-side">
                <SideNav navList={navList}></SideNav>
            </div>
            <div className="layout-content">
                {
                    children
                }
            </div>
        </div>
    )
}

export default Layout;