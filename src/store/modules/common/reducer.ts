import {GET_FEED, GET_USERINFO} from './actionCreator';

const defaultState = {
    userInfo: null,
    manager: false,
    feed: ''
}


const reducer = (state = defaultState, action: Action) => {
    switch (action.type) {
        case GET_USERINFO:
            return {...state, ...action.data};
        case GET_FEED:
            return {...state, ...action.data}
        default:
            return {...state};
    }
}


export default reducer;