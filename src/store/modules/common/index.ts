import * as actionCreator from './actionCreator';
import reducer from './reducer';

export default {
    actionCreator,
    reducer,
}