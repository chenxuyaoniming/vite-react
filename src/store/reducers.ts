import {combineReducers} from 'redux';
import Common from './modules/common';

const reducers = combineReducers({
    common: Common.reducer
});

export default reducers;