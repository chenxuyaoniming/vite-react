import { createStore, applyMiddleware, Store } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

const store: Store = createStore(
    reducers, 
    applyMiddleware(thunk)
);

export default store;